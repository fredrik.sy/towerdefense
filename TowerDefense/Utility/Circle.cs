﻿namespace TowerDefense.Utility
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Microsoft.Xna.Framework;
    using Microsoft.Xna.Framework.Graphics;

    internal class Circle : IDisposable
    {
        private Texture2D _texture;

        public Circle(GraphicsDevice graphics, int radius)
        {
            Radius = radius;
            CreateTexture(graphics);
        }

        public bool Visible { get; set; }

        public Color Color { get; set; } = Color.White;

        public Vector2 Center => new Vector2(X + Radius, Y + Radius);

        public Vector2 Origin { get; set; }

        public Vector2 Position
        {
            get => new Vector2(X, Y);
            set
            {
                X = value.X;
                Y = value.Y;
            }
        }

        public int Radius { get; set; }

        public float Alpha { get; set; } = 1;

        public float X { get; set; }

        public float Y { get; set; }

        public void Dispose()
        {
            _texture.Dispose();
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            if (Visible)
            {
                spriteBatch.Draw(_texture, Position, null, Color * Alpha, 0F, Origin, 1F, SpriteEffects.None, 0F);
            }
        }

        private void CreateTexture(GraphicsDevice graphics)
        {
            int diameter = 2 * Radius;
            _texture = new Texture2D(graphics, diameter, diameter);
            Color[] colorData = new Color[diameter * diameter];

            for (int y = -Radius; y < Radius; y++)
            {
                for (int x = -Radius; x < Radius; x++)
                {
                    int i = (x + Radius) + ((y + Radius) * diameter);

                    // Equation of the Circle.
                    double squaredLength = Math.Sqrt((x * x) + (y * y));

                    if (squaredLength > Radius - 1 && squaredLength <= Radius)
                    {
                        colorData[i] = Color.DarkGray;
                    }
                    else if (squaredLength <= Radius)
                    {
                        colorData[i] = Color.White;
                    }
                    else
                    {
                        colorData[i] = Color.Transparent;
                    }
                }
            }

            _texture.SetData(colorData);
        }
    }
}
