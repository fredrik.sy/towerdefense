﻿namespace TowerDefense.Objects
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Microsoft.Xna.Framework;
    using Microsoft.Xna.Framework.Graphics;
    using TowerDefense.Managers;
    using TowerDefense.Objects.Templates;

    internal class QuickBullet : Bullet
    {
        public const int HitDamage = 10;

        public QuickBullet()
        {
            Texture = TextureManager.Textures[typeof(QuickBullet)];
            HitEffect = typeof(Explosion);
            Speed = 250;
            Origin = new Vector2(2, 2);
        }

        public override int Damage
        {
            get => HitDamage;
        }

        public override Rectangle Hitbox
        {
            get => new Rectangle((int)(X - Origin.X), (int)(Y - Origin.Y), (int)(Texture.Width * Scale), (int)(Texture.Height * Scale));
        }
    }
}
