﻿namespace TowerDefense.Objects.Templates
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Microsoft.Xna.Framework;
    using Microsoft.Xna.Framework.Graphics;

    internal abstract class Tower : GameObject
    {
        public Enemy Target { get; set; }

        public float Cooldown { get; set; }

        public abstract int Cost { get; }

        public int Range { get; set; }

        public abstract Bullet CreateBullet();

        public override void Draw(SpriteBatch spriteBatch)
        {
            if (Visible)
            {
                spriteBatch.Draw(Texture, Position, new Rectangle(64, 0, 64, 64), Color.White * Alpha, 0F, Origin, Scale, SpriteEffects.None, LayerDepth);
                spriteBatch.Draw(Texture, Position, new Rectangle(128, 0, 64, 64), Color.White * Alpha, Rotation, Origin, Scale, SpriteEffects.None, LayerDepth);
            }
        }

        public override void Update(GameTime gameTime)
        {
            if (Target != null)
            {
                Vector2 distance = Target.Position - Position;

                if (distance.Length() <= Range && Target.Health > 0)
                {
                    Rotation = (float)Math.Atan2(distance.Y, distance.X);
                }
                else
                {
                    Target = null;
                }
            }

            if (Cooldown > 0)
            {
                Cooldown -= (int)gameTime.ElapsedGameTime.TotalMilliseconds;
            }
        }
    }
}
