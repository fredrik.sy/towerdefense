﻿namespace TowerDefense.Objects.Templates
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Microsoft.Xna.Framework;
    using Microsoft.Xna.Framework.Graphics;

    internal abstract class Bullet : GameObject
    {
        private Type _particleType;

        public Type HitEffect
        {
            get => _particleType;
            set
            {
                if (value.IsSubclassOf(typeof(Particle)))
                {
                    _particleType = value;
                }
                else
                {
                    throw new ArgumentException();
                }
            }
        }

        public virtual int Damage { get; }

        public float Speed { get; protected set; }

        public Vector2 Direction { get; set; }

        public Vector2 Velocity { get; set; }

        public Particle CreateParticle()
        {
            return Activator.CreateInstance(_particleType) as Particle;
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            if (Visible)
            {
                spriteBatch.Draw(Texture, Position, null, Color.White * Alpha, Rotation, Origin, Scale, SpriteEffects.None, LayerDepth);
            }
        }

        public override void Update(GameTime gameTime)
        {
            Velocity = Direction * Speed * (float)gameTime.ElapsedGameTime.TotalSeconds;
            Position += Velocity;
        }
    }
}
