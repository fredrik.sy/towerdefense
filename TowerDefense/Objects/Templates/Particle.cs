﻿namespace TowerDefense.Objects.Templates
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    internal abstract class Particle : GameObject
    {
        public float TimeToLive { get; set; }
    }
}
