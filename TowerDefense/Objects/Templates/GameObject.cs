﻿namespace TowerDefense.Objects.Templates
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Microsoft.Xna.Framework;
    using Microsoft.Xna.Framework.Graphics;

    internal abstract class GameObject
    {
        public bool Visible { get; set; } = true;

        public float Alpha { get; set; } = 1F;

        public float LayerDepth { get; set; }

        public float Rotation { get; set; }

        public float Scale { get; set; } = 1F;

        public virtual Rectangle Hitbox { get; set; }

        public Texture2D Texture { get; set; }

        public Matrix Transform
        {
            get => Matrix.CreateTranslation(new Vector3(-Origin, 0F)) * Matrix.CreateRotationZ(Rotation) * Matrix.CreateTranslation(new Vector3(Position, 0F));
        }

        public Vector2 Origin { get; set; }

        public Vector2 Position
        {
            get => new Vector2(X, Y);
            set
            {
                X = value.X;
                Y = value.Y;
            }
        }

        public float X { get; set; }

        public float Y { get; set; }

        public abstract void Draw(SpriteBatch spriteBatch);

        public abstract void Update(GameTime gameTime);
    }
}
