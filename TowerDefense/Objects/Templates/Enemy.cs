﻿namespace TowerDefense.Objects.Templates
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Microsoft.Xna.Framework;
    using Microsoft.Xna.Framework.Graphics;
    using Spline;

    internal abstract class Enemy : GameObject
    {
        public Enemy(SimplePath path)
        {
            Path = path;
        }

        public float PathPosition { get; set; }

        public float Speed { get; set; }

        public int Health { get; set; }

        public int Score { get; protected set; }

        public int Gold { get; protected set; }

        public SimplePath Path { get; set; }
    }
}
