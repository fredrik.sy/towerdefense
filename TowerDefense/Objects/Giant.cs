﻿namespace TowerDefense.Objects
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Microsoft.Xna.Framework;
    using Microsoft.Xna.Framework.Graphics;
    using Spline;
    using TowerDefense.Managers;
    using TowerDefense.Objects.Templates;

    internal class Giant : Enemy
    {
        private static readonly Vector2 HitboxOffset = new Vector2(13, 17);
        private static readonly Vector2 HitboxSize = new Vector2(26, 34);

        public Giant(SimplePath path) : base(path)
        {
            Texture = TextureManager.Textures[GetType()];
            Health = 100;
            Origin = new Vector2(Texture.Width / 2, Texture.Height / 2);
            Position = Path.GetPos(Path.beginT);
            Scale = 0.78125F;
            Score = 1;
            Gold = 2;
        }

        public override Rectangle Hitbox
        {
            get => new Rectangle((int)(X - HitboxOffset.X), (int)(Y - HitboxOffset.Y), (int)HitboxSize.X, (int)HitboxSize.Y);
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            if (Visible)
            {
                spriteBatch.Draw(Texture, Position, null, Color.White, Rotation, Origin, Scale, SpriteEffects.None, LayerDepth);
            }
        }

        public override void Update(GameTime gameTime)
        {
            if (PathPosition < Path.endT)
            {
                Vector2 lastPosition = Path.GetPos(PathPosition);
                PathPosition += Speed * (float)gameTime.ElapsedGameTime.TotalSeconds;
                Position = Path.GetPos(PathPosition);
                Vector2 distance = lastPosition - Position;
                Rotation = (float)Math.Atan2(distance.Y, distance.X);
            }
        }
    }
}
