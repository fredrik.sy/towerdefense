﻿namespace TowerDefense.Objects
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Microsoft.Xna.Framework;
    using Microsoft.Xna.Framework.Graphics;
    using TowerDefense.Managers;
    using TowerDefense.Objects.Templates;

    internal class CannonTower : Tower
    {
        public const int BuildCost = 75;

        private const int ReloadTimeInMilliseconds = 400;
        private static readonly Vector2 BulletPositionOffset = new Vector2(28, 0);

        public CannonTower()
        {
            Texture = TextureManager.Textures[GetType()];
        }

        public override int Cost
        {
            get => BuildCost;
        }

        public override Bullet CreateBullet()
        {
            if (Cooldown > 0)
            {
                return null;
            }

            Cooldown = ReloadTimeInMilliseconds;

            Vector2 direction = new Vector2((float)Math.Cos(Rotation), (float)Math.Sin(Rotation));
            Matrix rotation = Matrix.CreateRotationZ(Rotation);
            Vector2 bulletPositionOffset = Vector2.Transform(BulletPositionOffset, rotation);

            return new Cannonball()
            {
                Direction = direction,
                Position = Position + bulletPositionOffset,
                Rotation = Rotation
            };
        }
    }
}
