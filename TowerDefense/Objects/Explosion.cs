﻿namespace TowerDefense.Objects
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Microsoft.Xna.Framework;
    using Microsoft.Xna.Framework.Graphics;
    using TowerDefense.Managers;
    using TowerDefense.Objects.Templates;

    internal class Explosion : Particle
    {
        private const float TimeBeforeRemovalInMilliseconds = 100;

        public Explosion()
        {
            Texture = TextureManager.Textures[GetType()];
            Origin = new Vector2(Texture.Width / 2, Texture.Height / 2);
            TimeToLive = TimeBeforeRemovalInMilliseconds;
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            if (Visible)
            {
                spriteBatch.Draw(Texture, Position, null, Color.White * Alpha, 0F, Origin, Scale, SpriteEffects.None, LayerDepth);
            }
        }

        public override void Update(GameTime gameTime)
        {
            TimeToLive -= (float)gameTime.ElapsedGameTime.TotalMilliseconds;

            Scale = TimeToLive / TimeBeforeRemovalInMilliseconds;

            if (TimeToLive <= 0)
            {
                Visible = false;
            }
        }
    }
}
