﻿namespace TowerDefense.Objects
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Microsoft.Xna.Framework;
    using Microsoft.Xna.Framework.Graphics;
    using TowerDefense.Managers;
    using TowerDefense.Objects.Templates;

    internal class Cannonball : Bullet
    {
        public const int HitDamage = 20;

        public Cannonball()
        {
            Texture = TextureManager.Textures[typeof(Cannonball)];
            HitEffect = typeof(Explosion);
            Speed = 200;
            Origin = new Vector2(10, 6);
        }

        public override int Damage
        {
            get => HitDamage;
        }

        public override Rectangle Hitbox
        {
            get => new Rectangle((int)(X - Origin.X), (int)(Y - Origin.Y), (int)(Texture.Width * Scale), (int)(Texture.Height * Scale));
        }
    }
}
