﻿namespace TowerDefense.Managers.Templates
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Microsoft.Xna.Framework;
    using Microsoft.Xna.Framework.Graphics;
    using TowerDefense.Scenes;

    internal abstract class Manager
    {
        public Manager(GameplayScene gameplayScene)
        {
            GameplayScene = gameplayScene;
        }

        protected GameplayScene GameplayScene { get; private set; }

        public abstract void Draw(SpriteBatch spriteBatch);

        public abstract void Initialize();

        public abstract void LoadContent();

        public abstract void Update(GameTime gameTime);
    }
}
