﻿namespace TowerDefense.Managers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Microsoft.Xna.Framework;
    using Microsoft.Xna.Framework.Graphics;
    using TowerDefense.Managers.Templates;
    using TowerDefense.Objects.Templates;
    using TowerDefense.Scenes;

    internal class ParticleManager : Manager
    {
        private List<Particle> _particles;

        public ParticleManager(GameplayScene gameplayScene) : base(gameplayScene)
        {
            _particles = new List<Particle>();
        }

        public void Add(Particle particle)
        {
            _particles.Add(particle);
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            _particles.ForEach(e => e.Draw(spriteBatch));
        }

        public override void Initialize()
        {
        }

        public override void LoadContent()
        {
        }

        public override void Update(GameTime gameTime)
        {
            Particle[] particles = _particles.ToArray();

            foreach (Particle particle in particles)
            {
                if (particle.TimeToLive <= 0)
                {
                    _particles.Remove(particle);
                }
                else
                {
                    particle.Update(gameTime);
                }
            }
        }
    }
}
