﻿namespace TowerDefense.Managers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Microsoft.Xna.Framework;
    using Microsoft.Xna.Framework.Graphics;
    using TowerDefense.Managers.Templates;
    using TowerDefense.Objects;
    using TowerDefense.Objects.Templates;
    using TowerDefense.Scenes;

    internal class TowerManager : Manager
    {
        private List<Tower> _towers;

        public TowerManager(GameplayScene gameplayScene) : base(gameplayScene)
        {
            _towers = new List<Tower>();
        }

        public void Add(Tower tower)
        {
            tower.Alpha = 1F;
            _towers.Add(tower);
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            _towers.ForEach(e => e.Draw(spriteBatch));
        }

        public override void Initialize()
        {
            BulletManager bulletManager = GameplayScene.GetManager<BulletManager>();
            WaveManager waveManager = GameplayScene.GetManager<WaveManager>();

            if (bulletManager == null || waveManager == null)
            {
                throw new MissingMemberException();
            }
        }

        public override void LoadContent()
        {
        }

        public void Remove(Tower tower)
        {
        }

        public override void Update(GameTime gameTime)
        {
            BulletManager bulletManager = GameplayScene.GetManager<BulletManager>();
            WaveManager waveManager = GameplayScene.GetManager<WaveManager>();

            foreach (Tower tower in _towers)
            {
                if (tower.Target == null)
                {
                    foreach (Enemy enemy in waveManager.Enemies)
                    {
                        if (Vector2.Distance(tower.Position, enemy.Position) <= tower.Range)
                        {
                            tower.Target = enemy;
                            break;
                        }
                    }
                }

                tower.Update(gameTime);

                if (tower.Target != null)
                {
                    if (tower.Cooldown <= 0)
                    {
                        bulletManager.Add(tower.CreateBullet());
                    }
                }
            }
        }
    }
}
