﻿namespace TowerDefense.Managers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Microsoft.Xna.Framework;
    using Microsoft.Xna.Framework.Graphics;
    using TowerDefense.Managers.Templates;
    using TowerDefense.Scenes;

    internal class PlayerManager : Manager
    {
        private int _life;
        private int _gold;
        private int _score;

        public PlayerManager(GameplayScene gameplayScene) : base(gameplayScene)
        {
        }

        public event EventHandler LifeChanged;

        public event EventHandler GoldChanged;

        public event EventHandler ScoreChanged;

        public int Life
        {
            get => _life;
            set
            {
                _life = value > 0 ? value : 0;
                OnLifeChanged(new EventArgs());
            }
        }

        public int Gold
        {
            get => _gold;
            set
            {
                _gold = value > 0 ? value : 0;
                OnGoldChanged(new EventArgs());
            }
        }

        public int Score
        {
            get => _score;
            set
            {
                _score = value > 0 ? value : 0;
                OnScoreChanged(new EventArgs());
            }
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
        }

        public override void Initialize()
        {
        }

        public override void LoadContent()
        {
            Life = 10;
            Gold = 100;
            Score = 0;
        }

        public override void Update(GameTime gameTime)
        {
        }

        private void OnLifeChanged(EventArgs e)
        {
            LifeChanged?.Invoke(this, e);
        }

        private void OnGoldChanged(EventArgs e)
        {
            GoldChanged?.Invoke(this, e);
        }

        private void OnScoreChanged(EventArgs e)
        {
            ScoreChanged?.Invoke(this, e);
        }
    }
}
