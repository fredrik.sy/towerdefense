﻿namespace TowerDefense.Managers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Microsoft.Xna.Framework.Graphics;

    internal static class TextureManager
    {
        public static Dictionary<Type, Texture2D> Textures { get; } = new Dictionary<Type, Texture2D>();
    }
}
