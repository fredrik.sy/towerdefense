﻿namespace TowerDefense.Managers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Microsoft.Xna.Framework;
    using Microsoft.Xna.Framework.Graphics;
    using Spline;
    using TowerDefense.Managers.Templates;
    using TowerDefense.Objects;
    using TowerDefense.Objects.Templates;
    using TowerDefense.Scenes;

    internal class WaveManager : Manager
    {
        private const int WaveSpawnIntervalInSeconds = 15;
        private const int EnemySpawnIntervalInMilliseconds = 1000;

        private const float DifficultyScaling = 1.5F;
        private const float EnemySpeedScaling = 1.1F;

        private List<Enemy> _enemies;
        private WaveState _waveState;
        private float _enemySpeed;

        private float _millisecondsBeforeEnemySpawn;
        private float _secondsBeforeWaveSpawn;
        private int _spawnedEnemies;
        private int _maxEnemies;

        public WaveManager(GameplayScene gameplayScene) : base(gameplayScene)
        {
            _enemies = new List<Enemy>();
            _enemySpeed = 50;
            _maxEnemies = 10;
            _secondsBeforeWaveSpawn = WaveSpawnIntervalInSeconds;
            _waveState = WaveState.Inactive;
        }

        public event EventHandler WaveCountdownChanged;

        private enum WaveState
        {
            Active,
            Inactive,
            Ongoing
        }

        public int KilledEnemies { get; private set; }

        public int WavesCompleted { get; private set; }

        public Enemy[] Enemies
        {
            get => _enemies.ToArray();
        }

        public int WaveCountdown
        {
            get => (int)_secondsBeforeWaveSpawn;
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            _enemies.ForEach(e => e.Draw(spriteBatch));
        }

        public override void Initialize()
        {
            MapManager mapManager = GameplayScene.GetManager<MapManager>();
            PlayerManager playerManager = GameplayScene.GetManager<PlayerManager>();

            if (mapManager == null || playerManager == null)
            {
                throw new MissingMemberException();
            }
        }

        public override void LoadContent()
        {
        }

        public override void Update(GameTime gameTime)
        {
            switch (_waveState)
            {
                case WaveState.Active:
                    ReleaseEnemies(gameTime);
                    UpdateEnemies(gameTime);
                    break;
                case WaveState.Inactive:
                    UpdateWaveCountdown(gameTime);
                    break;
                case WaveState.Ongoing:
                    UpdateEnemies(gameTime);
                    break;
            }

            _enemies.ForEach(e => e.Update(gameTime));
        }

        private void OnWaveCountdownChanged(EventArgs e)
        {
            WaveCountdownChanged?.Invoke(this, e);
        }

        private void ReleaseEnemies(GameTime gameTime)
        {
            if (_spawnedEnemies < _maxEnemies)
            {
                _millisecondsBeforeEnemySpawn -= (float)gameTime.ElapsedGameTime.TotalMilliseconds;

                if (_millisecondsBeforeEnemySpawn <= 0)
                {
                    _millisecondsBeforeEnemySpawn += EnemySpawnIntervalInMilliseconds;

                    _enemies.Add(new Giant(GameplayScene.GetManager<MapManager>().SimplePath)
                    {
                        Speed = _enemySpeed
                    });

                    _spawnedEnemies++;
                }
            }
            else
            {
                _waveState = WaveState.Ongoing;
                _enemySpeed *= EnemySpeedScaling;
                _maxEnemies = (int)(_maxEnemies * DifficultyScaling);
                _spawnedEnemies = 0;
            }
        }

        private void UpdateEnemies(GameTime gameTime)
        {
            if (_enemies.Count == 0 && _waveState == WaveState.Ongoing)
            {
                WavesCompleted++;
                _waveState = WaveState.Inactive;
                _secondsBeforeWaveSpawn = WaveSpawnIntervalInSeconds;
            }
            else
            {
                Enemy[] enemies = _enemies.ToArray();
                PlayerManager playerManager = GameplayScene.GetManager<PlayerManager>();

                foreach (Enemy enemy in enemies)
                {
                    if (enemy.PathPosition < enemy.Path.endT)
                    {
                        if (enemy.Health <= 0)
                        {
                            KilledEnemies++;
                            _enemies.Remove(enemy);
                            playerManager.Score += enemy.Score;
                            playerManager.Gold += enemy.Gold;
                        }
                    }
                    else
                    {
                        _enemies.Remove(enemy);
                        playerManager.Life -= 1;
                    }
                }
            }
        }

        private void UpdateWaveCountdown(GameTime gameTime)
        {
            _secondsBeforeWaveSpawn -= (float)gameTime.ElapsedGameTime.TotalSeconds;

            if (_secondsBeforeWaveSpawn < 0)
            {
                _waveState = WaveState.Active;
                _secondsBeforeWaveSpawn = 0;
            }

            OnWaveCountdownChanged(new EventArgs());
        }
    }
}
