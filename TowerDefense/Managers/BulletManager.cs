﻿namespace TowerDefense.Managers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Microsoft.Xna.Framework;
    using Microsoft.Xna.Framework.Graphics;
    using TowerDefense.Managers.Templates;
    using TowerDefense.Objects;
    using TowerDefense.Objects.Templates;
    using TowerDefense.Scenes;
    using TowerDefense.Utility;

    internal class BulletManager : Manager
    {
        private List<Bullet> _bullets;

        public BulletManager(GameplayScene gameplayScene) : base(gameplayScene)
        {
            _bullets = new List<Bullet>();
        }

        public void Add(Bullet bullet)
        {
            _bullets.Add(bullet);
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            _bullets.ForEach(e => e.Draw(spriteBatch));
        }

        public override void Initialize()
        {
            ParticleManager particleManager = GameplayScene.GetManager<ParticleManager>();
            WaveManager waveManager = GameplayScene.GetManager<WaveManager>();

            if (particleManager == null || waveManager == null)
            {
                throw new MissingMemberException();
            }
        }

        public override void LoadContent()
        {
        }

        public override void Update(GameTime gameTime)
        {
            Bullet[] bullets = _bullets.ToArray();
            Enemy[] enemies = GameplayScene.GetManager<WaveManager>().Enemies;
            ParticleManager particleManager = GameplayScene.GetManager<ParticleManager>();

            foreach (Bullet bullet in bullets)
            {
                foreach (Enemy enemy in enemies)
                {
                    Rectangle enemyHitbox = enemy.Hitbox;
                    Rectangle bulletHitbox = bullet.Hitbox;

                    if (enemyHitbox.Contains(bulletHitbox) || enemyHitbox.Intersects(bulletHitbox))
                    {
                        _bullets.Remove(bullet);

                        bulletHitbox.Inflate(5, 5);
                        Rectangle particleBounds = Rectangle.Intersect(enemyHitbox, bulletHitbox);

                        Particle particle = bullet.CreateParticle();
                        particle.Position = new Vector2(Generator.Random.Next(particleBounds.X, particleBounds.Right), Generator.Random.Next(particleBounds.Y, particleBounds.Bottom));
                        particleManager.Add(particle);

                        enemy.Health -= bullet.Damage;
                    }
                }

                bullet.Update(gameTime);

                if (!GameplayScene.WindowBounds.Contains(bullet.Hitbox) && !GameplayScene.WindowBounds.Intersects(bullet.Hitbox))
                {
                    _bullets.Remove(bullet);
                }
            }
        }
    }
}
