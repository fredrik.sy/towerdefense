﻿namespace TowerDefense.Managers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Microsoft.Xna.Framework;
    using Microsoft.Xna.Framework.Graphics;
    using RogueSharp.MapCreation;
    using Spline;
    using TowerDefense.Managers.Templates;
    using TowerDefense.Map;
    using TowerDefense.Scenes;
    using Cell = RogueSharp.Cell;
    using File = System.IO.File;
    using Path = RogueSharp.Path;
    using PathFinder = RogueSharp.PathFinder;

    internal class MapManager : Manager
    {
        public const int CellSize = 50;

        private Texture2D _grass;
        private Texture2D _ground;
        private ITdMap _tdMap;

        public MapManager(GameplayScene gameplayScene, string path) : base(gameplayScene)
        {
            ReadMap(path);
        }

        public SimplePath SimplePath { get; private set; }

        public int Height
        {
            get => (_tdMap.ScreenSize.Y + _tdMap.Offset.Y) * CellSize;
        }

        public int Width
        {
            get => (_tdMap.ScreenSize.X + _tdMap.Offset.X) * CellSize;
        }

        public bool Contains(Point value)
        {
            return value.X >= 0 && value.X <= Width && value.Y >= 0 && value.Y <= Height;
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            foreach (Cell cell in _tdMap.GetAllCells())
            {
                if (!cell.IsTransparent)
                {
                    if (cell.IsWalkable)
                    {
                        Rectangle destination = new Rectangle((cell.X + _tdMap.Offset.X) * CellSize, (cell.Y + _tdMap.Offset.Y) * CellSize, CellSize, CellSize);
                        spriteBatch.Draw(_ground, destination, Color.White);
                    }
                    else
                    {
                        Rectangle destination = new Rectangle((cell.X + _tdMap.Offset.X) * CellSize, (cell.Y + _tdMap.Offset.Y) * CellSize, CellSize, CellSize);
                        spriteBatch.Draw(_grass, destination, Color.White);
                    }
                }
            }
        }

        public TdCell GetTdCell(int x, int y)
        {
            return _tdMap.GetTdCell((x / CellSize) - _tdMap.Offset.X, (y / CellSize) - _tdMap.Offset.Y);
        }

        public Vector2 GetTdCellScreenPosition(TdCell tdCell)
        {
            return new Vector2((tdCell.X + _tdMap.Offset.X) * CellSize, (tdCell.Y + _tdMap.Offset.Y) * CellSize);
        }

        public void SetTdCell(int x, int y, bool? isTransparent = null, bool? isWalkable = null, bool? isOccupied = null)
        {
            _tdMap.SetTdCellProperties(x, y, isTransparent, isWalkable, isOccupied: isOccupied);
        }

        public override void Initialize()
        {
            InitializeSimplePath();
        }

        public override void LoadContent()
        {
            _grass = GameplayScene.Game.Content.Load<Texture2D>(@"Tiles\Grass");
            _ground = GameplayScene.Game.Content.Load<Texture2D>(@"Tiles\Ground");
        }

        public override void Update(GameTime gameTime)
        {
        }

        private void InitializeSimplePath()
        {
            SimplePath = new SimplePath(GameplayScene.GraphicsDevice);
            SimplePath.Clean();

            Cell beginT = _tdMap.FindBeginT();
            Cell endT = _tdMap.FindEndT();
            PathFinder pathFinder = new PathFinder(_tdMap);
            Path path = pathFinder.ShortestPath(beginT, endT);

            SimplePath.AddPoint(new Vector2(((beginT.X + _tdMap.Offset.X) * CellSize) + (CellSize / 2), ((beginT.Y + _tdMap.Offset.Y) * CellSize) + (CellSize / 2)));

            foreach (Cell cell in path.Steps)
            {
                SimplePath.AddPoint(new Vector2(((cell.X + _tdMap.Offset.X) * CellSize) + (CellSize / 2), ((cell.Y + _tdMap.Offset.Y) * CellSize) + (CellSize / 2)));
            }
        }

        private void ReadMap(string path)
        {
            string mapRepresentation = File.ReadAllText(path);
            IMapCreationStrategy<TdMap> mapCreationStrategy = new StringDeserializeTdMapCreationStrategy<TdMap>(mapRepresentation);
            _tdMap = TdMap.Create(mapCreationStrategy);
        }
    }
}
