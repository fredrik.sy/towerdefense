﻿namespace TowerDefense.Managers
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using GeonBit.UI;
    using GeonBit.UI.Entities;
    using Microsoft.Xna.Framework;
    using Microsoft.Xna.Framework.Graphics;
    using TowerDefense.IO;
    using TowerDefense.Managers.Templates;
    using TowerDefense.Map;
    using TowerDefense.Objects;
    using TowerDefense.Objects.Templates;
    using TowerDefense.Scenes;
    using TowerDefense.Utility;

    internal class UiManager : Manager
    {
        private static readonly Vector2 TowerIconSize = new Vector2(30, 30);

        private Circle _towerRange;
        private Tower _towerPlacement;

        private Paragraph _life;
        private Paragraph _gold;
        private Paragraph _score;
        private Paragraph _waveTime;

        public UiManager(GameplayScene gameplayScene) : base(gameplayScene)
        {
        }

        public override void Initialize()
        {
            MapManager mapManager = GameplayScene.GetManager<MapManager>();
            TowerManager towerManager = GameplayScene.GetManager<TowerManager>();
            WaveManager waveManager = GameplayScene.GetManager<WaveManager>();
            PlayerManager playerManager = GameplayScene.GetManager<PlayerManager>();

            if (mapManager == null || towerManager == null || waveManager == null || playerManager == null)
            {
                throw new MissingMemberException();
            }

            waveManager.WaveCountdownChanged += WaveManager_WaveCountdownChanged;
            playerManager.GoldChanged += PlayerManager_GoldChanged;
            playerManager.LifeChanged += PlayerManager_LifeChanged;
            playerManager.ScoreChanged += PlayerManager_ScoreChanged;
            GameplayScene.WindowSize = new Point(mapManager.Width + 200, mapManager.Height);
        }

        public override void Update(GameTime gameTime)
        {
            if (_towerPlacement != null)
            {
                Point mousePosition = MouseReader.Position;
                MapManager mapManager = GameplayScene.GetManager<MapManager>();

                if (mapManager.Contains(mousePosition))
                {
                    int mousePositionX = MathHelper.Clamp(mousePosition.X, 0, mapManager.Width - 1);
                    int mousePositionY = MathHelper.Clamp(mousePosition.Y, 0, mapManager.Height - 1);

                    TdCell tdCell = mapManager.GetTdCell(mousePositionX, mousePositionY);
                    _towerPlacement.Position = mapManager.GetTdCellScreenPosition(tdCell) + new Vector2(MapManager.CellSize / 2, MapManager.CellSize / 2);
                    _towerPlacement.Visible = true;
                    _towerRange.Visible = true;
                    _towerRange.X = _towerPlacement.X;
                    _towerRange.Y = _towerPlacement.Y;

                    if (tdCell.IsOccupied || tdCell.IsWalkable)
                    {
                        _towerRange.Color = Color.DarkRed;
                    }
                    else
                    {
                        _towerRange.Color = Color.White;

                        if (MouseReader.LeftClick())
                        {
                            PlayerManager playerManager = GameplayScene.GetManager<PlayerManager>();
                            playerManager.Gold -= _towerPlacement.Cost;
                            TowerManager towerManager = GameplayScene.GetManager<TowerManager>();
                            towerManager.Add(_towerPlacement);
                            mapManager.SetTdCell(tdCell.X, tdCell.Y, isOccupied: true);
                            _towerPlacement = null;
                            _towerRange = null;
                        }
                    }
                }
                else
                {
                    _towerPlacement.Visible = false;
                    _towerRange.Visible = false;
                }
            }

            if (MouseReader.RightClick())
            {
                _towerPlacement = null;
                _towerRange = null;
            }
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            _towerRange?.Draw(spriteBatch);
            _towerPlacement?.Draw(spriteBatch);
        }

        public override void LoadContent()
        {
            Panel mainPanel = new Panel(new Vector2(200, 0), PanelSkin.Simple, Anchor.TopRight)
            {
                Padding = Vector2.Zero
            };

            mainPanel.AddChild(CreatePlayerInfoPanel());
            mainPanel.AddChild(CreateTowerPanel());

            UserInterface.Active.AddEntity(mainPanel);
        }

        private Panel CreatePlayerInfoPanel()
        {
            Panel playerInfoPanel = new Panel(new Vector2(0, 105), PanelSkin.Simple, Anchor.TopLeft)
            {
                Padding = Vector2.Zero
            };

            _life = new Paragraph("Life: 0", offset: new Vector2(10, 10), scale: 0.6F);
            _gold = new Paragraph("Gold: 0", offset: new Vector2(10, 0), scale: 0.6F);
            _score = new Paragraph("Score: 0", offset: new Vector2(10, 0), scale: 0.6F);
            _waveTime = new Paragraph("Next Wave Time: ", offset: new Vector2(10, 0), scale: 0.6F);

            playerInfoPanel.AddChild(_life);
            playerInfoPanel.AddChild(_gold);
            playerInfoPanel.AddChild(_score);
            playerInfoPanel.AddChild(_waveTime);

            return playerInfoPanel;
        }

        private Panel CreateTowerPanel()
        {
            Panel towerPanel = new Panel(new Vector2(0, 0), PanelSkin.Simple, Anchor.TopLeft, new Vector2(0, 105))
            {
                Padding = new Vector2(10, 10)
            };

            Header towerHeader = new Header("Towers")
            {
                Scale = 0.7F
            };

            Panel towerInfoPanel = new Panel(new Vector2(0, 100), PanelSkin.ListBackground, Anchor.BottomCenter, new Vector2(0, 105))
            {
                Padding = new Vector2(15, 10)
            };

            Header towerInfoHeader = new Header(string.Empty)
            {
                Scale = 0.6F
            };

            Paragraph towerInfoParagraph = new Paragraph(string.Empty, scale: 0.6F);

            Icon gunTowerIcon = new Icon(IconType.None, Anchor.Auto, background: true, offset: new Vector2(5, 0))
            {
                Size = TowerIconSize,
                SourceRectangle = new Rectangle(0, 0, 64, 64),
                Texture = TextureManager.Textures[typeof(GunTower)],
                OnMouseEnter = (Entity sender) => { GunTowerIcon_OnMouseEnter(sender, towerInfoHeader, towerInfoParagraph); },
                OnClick = (Entity sender) => { GunTowerIcon_OnClick(sender); },
                OnMouseLeave = (Entity sender) => { GunTowerIcon_OnMouseLeave(sender, towerInfoHeader, towerInfoParagraph); }
            };

            Icon cannonTowerIcon = new Icon(IconType.None, Anchor.AutoInline, background: true)
            {
                Size = TowerIconSize,
                SourceRectangle = new Rectangle(0, 0, 64, 64),
                Texture = TextureManager.Textures[typeof(CannonTower)],
                OnMouseEnter = (Entity sender) => { CannonTowerIcon_OnMouseEnter(sender, towerInfoHeader, towerInfoParagraph); },
                OnClick = (Entity sender) => { CannonTowerIcon_OnClick(sender); },
                OnMouseLeave = (Entity sender) => { CannonTowerIcon_OnMouseLeave(sender, towerInfoHeader, towerInfoParagraph); }
            };

            towerInfoPanel.AddChild(towerInfoHeader);
            towerInfoPanel.AddChild(towerInfoParagraph);

            towerPanel.AddChild(towerHeader);
            towerPanel.AddChild(new HorizontalLine());
            towerPanel.AddChild(gunTowerIcon);
            towerPanel.AddChild(cannonTowerIcon);
            towerPanel.AddChild(towerInfoPanel);

            return towerPanel;
        }

        #region CannonTowerIcon Events
        private void CannonTowerIcon_OnClick(Entity sender)
        {
            PlayerManager playerManager = GameplayScene.GetManager<PlayerManager>();

            if (playerManager.Gold >= CannonTower.BuildCost)
            {
                _towerPlacement = new CannonTower()
                {
                    Alpha = 0.5F,
                    Origin = new Vector2(32, 32),
                    Range = 150,
                    Scale = 0.78125F,
                    Visible = false
                };
                _towerRange = new Circle(GameplayScene.GraphicsDevice, _towerPlacement.Range)
                {
                    Alpha = 0.5F,
                    Origin = new Vector2(_towerPlacement.Range, _towerPlacement.Range)
                };
            }
        }

        private void CannonTowerIcon_OnMouseEnter(Entity sender, Header header, Paragraph paragraph)
        {
            header.Text = "Cannon Tower";
            paragraph.Text = string.Format("Cost: {0}G\nDamage: {1}", CannonTower.BuildCost, Cannonball.HitDamage);
        }

        private void CannonTowerIcon_OnMouseLeave(Entity sender, Header header, Paragraph paragraph)
        {
            header.Text = string.Empty;
            paragraph.Text = string.Empty;
        }
        #endregion

        #region GunTowerIcon Events
        private void GunTowerIcon_OnClick(Entity sender)
        {
            PlayerManager playerManager = GameplayScene.GetManager<PlayerManager>();

            if (playerManager.Gold >= GunTower.BuildCost)
            {
                _towerPlacement = new GunTower()
                {
                    Alpha = 0.5F,
                    Origin = new Vector2(32, 32),
                    Range = 100,
                    Scale = 0.78125F,
                    Visible = false
                };
                _towerRange = new Circle(GameplayScene.GraphicsDevice, _towerPlacement.Range)
                {
                    Alpha = 0.5F,
                    Origin = new Vector2(_towerPlacement.Range, _towerPlacement.Range)
                };
            }
        }

        private void GunTowerIcon_OnMouseEnter(Entity sender, Header header, Paragraph paragraph)
        {
            header.Text = "Gun Tower";
            paragraph.Text = string.Format("Cost: {0}G\nDamage: {1}", GunTower.BuildCost, QuickBullet.HitDamage);
        }

        private void GunTowerIcon_OnMouseLeave(Entity sender, Header header, Paragraph paragraph)
        {
            header.Text = string.Empty;
            paragraph.Text = string.Empty;
        }
        #endregion

        #region PlayerManager Events
        private void PlayerManager_GoldChanged(object sender, EventArgs e)
        {
            PlayerManager playerManager = sender as PlayerManager;
            _gold.Text = string.Format("Gold: {0}", playerManager.Gold);
        }

        private void PlayerManager_LifeChanged(object sender, EventArgs e)
        {
            PlayerManager playerManager = sender as PlayerManager;
            _life.Text = string.Format("Life: {0}", playerManager.Life);
        }

        private void PlayerManager_ScoreChanged(object sender, EventArgs e)
        {
            PlayerManager playerManager = sender as PlayerManager;
            _score.Text = string.Format("Score: {0}", playerManager.Score);
        }
        #endregion

        #region WaveManager Events
        private void WaveManager_WaveCountdownChanged(object sender, EventArgs e)
        {
            WaveManager waveManager = sender as WaveManager;
            _waveTime.Text = string.Format("Next Wave Time: {0}", waveManager.WaveCountdown);
        }
        #endregion
    }
}
