﻿namespace TowerDefense.Map
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Microsoft.Xna.Framework;
    using RogueSharp.MapCreation;
    using Cell = RogueSharp.Cell;
    using Map = RogueSharp.Map;

    public class TdMap : Map, ITdMap
    {
        private bool[,] _isBeginT;
        private bool[,] _isEndT;
        private bool[,] _isOccupied;

        private int _minScreenX;
        private int _minScreenY;
        private int _maxScreenX;
        private int _maxScreenY;

        public TdMap() : base()
        {
            _minScreenX = int.MaxValue;
            _minScreenY = int.MaxValue;
            _maxScreenX = int.MinValue;
            _maxScreenY = int.MinValue;
        }

        public TdMap(int width, int height) : base(width, height)
        {
            _isBeginT = new bool[width, height];
            _isEndT = new bool[width, height];
            _isOccupied = new bool[width, height];
        }

        public Point Offset { get; set; } = Point.Zero;

        public Point ScreenSize
        {
            get => new Point(_maxScreenX - _minScreenX + 1, _maxScreenY - _minScreenY + 1);
        }

        public static TdMap Create(IMapCreationStrategy<TdMap> mapCreationStrategy)
        {
            if (mapCreationStrategy == null)
            {
                throw new ArgumentNullException("mapCreationStrategy", "Map creation strategy cannot be null");
            }

            return mapCreationStrategy.CreateMap();
        }

        public Cell FindBeginT()
        {
            for (int y = 0; y < _isBeginT.GetLength(1); y++)
            {
                for (int x = 0; x < _isBeginT.GetLength(0); x++)
                {
                    if (_isBeginT[x, y])
                    {
                        return GetCell(x, y);
                    }
                }
            }

            return null;
        }

        public Cell FindEndT()
        {
            for (int y = 0; y < _isEndT.GetLength(1); y++)
            {
                for (int x = 0; x < _isEndT.GetLength(0); x++)
                {
                    if (_isEndT[x, y])
                    {
                        return GetCell(x, y);
                    }
                }
            }

            return null;
        }

        public TdCell GetTdCell(int x, int y)
        {
            return new TdCell(GetCell(x, y))
            {
                IsOccupied = _isOccupied[x, y]
            };
        }

        public new void Initialize(int width, int height)
        {
            _isBeginT = new bool[width, height];
            _isEndT = new bool[width, height];
            _isOccupied = new bool[width, height];
            base.Initialize(width, height);
        }

        public void SetTdCellProperties(int x, int y, bool? isTransparent = null, bool? isWalkable = null, bool? isBeginT = null, bool? isEndT = null, bool? isOccupied = null)
        {
            bool transparent = isTransparent.GetValueOrDefault(IsTransparent(x, y));
            bool walkable = isWalkable.GetValueOrDefault(IsWalkable(x, y));
            bool beginT = isBeginT.GetValueOrDefault(_isBeginT[x, y]);
            bool endT = isEndT.GetValueOrDefault(_isEndT[x, y]);
            bool occupied = isOccupied.GetValueOrDefault(_isOccupied[x, y]);

            if (!transparent)
            {
                if (_minScreenX > x)
                {
                    _minScreenX = x;
                }

                if (_minScreenY > y)
                {
                    _minScreenY = y;
                }

                if (_maxScreenX < x)
                {
                    _maxScreenX = x;
                }

                if (_maxScreenY < y)
                {
                    _maxScreenY = y;
                }
            }

            _isBeginT[x, y] = beginT;
            _isEndT[x, y] = endT;
            _isOccupied[x, y] = occupied;
            SetCellProperties(x, y, transparent, walkable);
        }
    }
}
