﻿namespace TowerDefense.Map
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Microsoft.Xna.Framework;
    using Cell = RogueSharp.Cell;
    using IMap = RogueSharp.IMap;

    internal interface ITdMap : IMap
    {
        Point Offset { get; set; }

        Point ScreenSize { get; }

        Cell FindBeginT();

        Cell FindEndT();

        TdCell GetTdCell(int x, int y);

        void SetTdCellProperties(int x, int y, bool? isTransparent = null, bool? isWalkable = null, bool? isBeginT = null, bool? isEndT = null, bool? isOccupied = null);
    }
}
