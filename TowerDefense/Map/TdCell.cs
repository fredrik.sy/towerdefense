﻿namespace TowerDefense.Map
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Cell = RogueSharp.Cell;

    public class TdCell : Cell
    {
        public TdCell(Cell cell) : base(cell.X, cell.Y, cell.IsTransparent, cell.IsWalkable, cell.IsInFov, cell.IsExplored)
        {
        }

        public bool IsOccupied { get; set; }
    }
}
