/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 - 2017 Faron Bracy
 * https://bitbucket.org/FaronBracy/roguesharp
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

namespace TowerDefense.Map
{
    using System.Collections.Generic;
    using Microsoft.Xna.Framework;
    using RogueSharp.MapCreation;
    using Cell = RogueSharp.Cell;

    /// <summary>
    /// The StringDeserializeMapCreationStrategy creates a Map of the specified type from a string representation of the Map
    /// </summary>
    /// <typeparam name="T">The type of IMap that will be created</typeparam>
    internal class StringDeserializeTdMapCreationStrategy<T> : IMapCreationStrategy<T> where T : ITdMap, new()
    {
        private readonly string _mapRepresentation;

        /// <summary>
        /// Initializes a new instance of the <see cref="StringDeserializeTdMapCreationStrategy{T}"/> class.
        /// </summary>
        /// <param name="mapRepresentation">A string representation of the Map to be created</param>
        public StringDeserializeTdMapCreationStrategy(string mapRepresentation)
        {
            _mapRepresentation = mapRepresentation;
        }

        /// <summary>
        /// Creates a Map of the specified type from a string representation of the Map
        /// </summary>
        /// <remarks>
        /// The following symbols represent Cells on the Map:
        /// - `.`: `Cell` is transparent and walkable
        /// - `s`: `Cell` is walkable (but not transparent)
        /// - `o`: `Cell` is transparent (but not walkable)
        /// - `#`: `Cell` is not transparent or walkable
        /// - `b`: `Cell` is beginT
        /// - `e`: `Cell` is endT
        /// </remarks>
        /// <returns>An IMap of the specified type</returns>
        public T CreateMap()
        {
            string[] lines = _mapRepresentation.Replace(" ", string.Empty).Replace("\r", string.Empty).Split('\n');

            int width = lines[0].Length;
            int height = lines.Length;
            var map = new T();
            map.Initialize(width, height);

            for (int y = 0; y < height; y++)
            {
                string line = lines[y];
                for (int x = 0; x < width; x++)
                {
                    if (line[x] == '.')
                    {
                        map.SetTdCellProperties(x, y, true, true, false, false);
                    }
                    else if (line[x] == 's')
                    {
                        map.SetTdCellProperties(x, y, false, true, false, false);
                    }
                    else if (line[x] == 'o')
                    {
                        map.SetTdCellProperties(x, y, true, false, false, false);
                    }
                    else if (line[x] == '#')
                    {
                        map.SetTdCellProperties(x, y, false, false, false, false);
                    }
                    else if (line[x] == 'b')
                    {
                        map.SetTdCellProperties(x, y, true, true, true, false);
                    }
                    else if (line[x] == 'e')
                    {
                        map.SetTdCellProperties(x, y, true, true, false, true);
                    }
                }
            }

            for (int y = 0; y < height; y++)
            {
                if (!IsCellsTransparent(map.GetCellsInRows(y)))
                {
                    Point offset = map.Offset;
                    offset.Y = -y;
                    map.Offset = offset;
                    break;
                }
            }

            for (int x = 0; x < height; x++)
            {
                if (!IsCellsTransparent(map.GetCellsInColumns(x)))
                {
                    Point offset = map.Offset;
                    offset.X = -x;
                    map.Offset = offset;
                    break;
                }
            }

            return map;
        }

        private bool IsCellsTransparent(IEnumerable<Cell> collection)
        {
            foreach (Cell cell in collection)
            {
                if (!cell.IsTransparent)
                {
                    return false;
                }
            }

            return true;
        }
    }
}