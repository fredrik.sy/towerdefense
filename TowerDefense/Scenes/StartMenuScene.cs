﻿namespace TowerDefense.Scenes
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using GeonBit.UI;
    using GeonBit.UI.Entities;
    using Microsoft.Xna.Framework;
    using Microsoft.Xna.Framework.Graphics;
    using TowerDefense.Scenes.Templates;

    internal class StartMenuScene : Scene
    {
        private SpriteBatch _spriteBatch;
        private Texture2D _background;

        public StartMenuScene(Game game) : base(game)
        {
        }

        public override void Draw(GameTime gameTime)
        {
            UserInterface.Active.Draw(_spriteBatch);

            GraphicsDevice.Clear(Color.CornflowerBlue);

            _spriteBatch.Begin();
            _spriteBatch.Draw(_background, Vector2.Zero, Color.White);
            _spriteBatch.End();

            UserInterface.Active.DrawMainRenderTarget(_spriteBatch);

            base.Draw(gameTime);
        }

        public override void Initialize()
        {
            WindowSize = new Point(800, 600);
            InitializeGeonBitButtons();
            base.Initialize();
        }

        public override void Update(GameTime gameTime)
        {
            if (!Game.IsActive)
            {
                return;
            }

            UserInterface.Active.Update(gameTime);

            base.Update(gameTime);
        }

        protected override void LoadContent()
        {
            _spriteBatch = new SpriteBatch(GraphicsDevice);
            _background = Game.Content.Load<Texture2D>(@"StartMenu\Background");

            base.LoadContent();
        }

        private void InitializeGeonBitButtons()
        {
            Button playButton = new Button("Play", ButtonSkin.Alternative, Anchor.Center, new Vector2(220, 50), new Vector2(0, 70))
            {
                OnClick = (Entity sender) => { PlayButton_OnClick(sender); }
            };

            Button exitButton = new Button("Exit", ButtonSkin.Alternative, Anchor.Center, new Vector2(220, 50), new Vector2(0, 160))
            {
                OnClick = (Entity sender) => { ExitButton_OnClick(sender); }
            };

            UserInterface.Active.AddEntity(playButton);
            UserInterface.Active.AddEntity(exitButton);
        }

        private void PlayButton_OnClick(Entity sender)
        {
            UserInterface.Active.Clear();
            Game.Components.Clear();
            Game.Components.Add(new MapSelectScene(Game));
        }

        private void ExitButton_OnClick(Entity sender)
        {
            Game.Exit();
        }
    }
}
