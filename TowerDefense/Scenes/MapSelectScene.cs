﻿namespace TowerDefense.Scenes
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using GeonBit.UI;
    using GeonBit.UI.Entities;
    using Microsoft.Xna.Framework;
    using Microsoft.Xna.Framework.Graphics;
    using TowerDefense.Managers;
    using TowerDefense.Scenes.Templates;

    internal class MapSelectScene : Scene
    {
        private const string MapDirectory = "Maps";
        private const string MapExtension = ".map";

        private SpriteBatch _spriteBatch;
        private Texture2D _hill;

        public MapSelectScene(Game game) : base(game)
        {
        }

        public override void Draw(GameTime gameTime)
        {
            UserInterface.Active.Draw(_spriteBatch);

            GraphicsDevice.Clear(Color.CornflowerBlue);

            _spriteBatch.Begin();
            _spriteBatch.Draw(_hill, Vector2.Zero, Color.White);
            _spriteBatch.End();

            UserInterface.Active.DrawMainRenderTarget(_spriteBatch);

            base.Draw(gameTime);
        }

        public override void Initialize()
        {
            WindowSize = new Point(800, 600);
            base.Initialize();
        }

        public override void Update(GameTime gameTime)
        {
            if (!Game.IsActive)
            {
                return;
            }

            UserInterface.Active.Update(gameTime);

            base.Update(gameTime);
        }

        protected override void LoadContent()
        {
            _spriteBatch = new SpriteBatch(Game.GraphicsDevice);
            _hill = Game.Content.Load<Texture2D>(@"LevelSelect\Hill");

            InitializeGeonBitUI();

            base.LoadContent();
        }

        private void InitializeGeonBitUI()
        {
            Button backButton = new Button("Back", ButtonSkin.Default, Anchor.TopLeft, new Vector2(140, 40), new Vector2(20, 20))
            {
                OnClick = (Entity sender) => { BackButton_OnClick(sender); }
            };

            backButton.ButtonParagraph.Scale = 0.7F;

            SelectList selectList = new SelectList();
            selectList.LockedItems[0] = true;
            selectList.AddItem(string.Format("{0}{1}", "{{RED}}", "Path"));

            if (Directory.Exists(MapDirectory))
            {
                foreach (string fileName in Directory.GetFiles(MapDirectory, '*' + MapExtension))
                {
                    selectList.AddItem(fileName);
                }
            }

            Button startGameButton = new Button("Start Game", ButtonSkin.Default, Anchor.BottomCenter, new Vector2(240, 50))
            {
                OnClick = (Entity sender) => { StartGameButton_OnClick(sender, selectList.SelectedValue); }
            };

            Panel panel = new Panel(new Vector2(600, 390), offset: new Vector2(0, 20));
            panel.AddChild(new Header("Select Map"));
            panel.AddChild(selectList);
            panel.AddChild(startGameButton);

            UserInterface.Active.AddEntity(backButton);
            UserInterface.Active.AddEntity(panel);
        }

        private void BackButton_OnClick(Entity sender)
        {
            UserInterface.Active.Clear();
            Game.Components.Clear();
            Game.Components.Add(new StartMenuScene(Game));
        }

        private void StartGameButton_OnClick(Entity sender, string selectedFileName)
        {
            if (!string.IsNullOrEmpty(selectedFileName))
            {
                UserInterface.Active.Clear();
                Game.Components.Clear();

                GameplayScene gameplayScene = new GameplayScene(Game, selectedFileName);
                Game.Components.Add(gameplayScene);
            }
        }
    }
}
