﻿namespace TowerDefense.Scenes
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using GeonBit.UI;
    using GeonBit.UI.Entities;
    using Microsoft.Xna.Framework;
    using Microsoft.Xna.Framework.Graphics;
    using TowerDefense.Scenes.Templates;

    internal class GameOverScene : Scene
    {
        private SpriteBatch _spriteBatch;
        private Texture2D _hill;

        public GameOverScene(Game game) : base(game)
        {
        }

        public int CreepsKilled { private get; set; }

        public int GoldEarned { private get; set; }

        public int Score { private get; set; }

        public int WavesCompleted { private get; set; }

        public override void Draw(GameTime gameTime)
        {
            UserInterface.Active.Draw(_spriteBatch);

            GraphicsDevice.Clear(Color.CornflowerBlue);

            _spriteBatch.Begin();
            _spriteBatch.Draw(_hill, Vector2.Zero, Color.White);
            _spriteBatch.End();

            UserInterface.Active.DrawMainRenderTarget(_spriteBatch);

            base.Draw(gameTime);
        }

        public override void Initialize()
        {
            WindowSize = new Point(800, 600);

            InitializeGeonBitUI();

            base.Initialize();
        }

        public override void Update(GameTime gameTime)
        {
            if (!Game.IsActive)
            {
                return;
            }

            UserInterface.Active.Update(gameTime);

            base.Update(gameTime);
        }

        protected override void LoadContent()
        {
            _spriteBatch = new SpriteBatch(Game.GraphicsDevice);
            _hill = Game.Content.Load<Texture2D>(@"LevelSelect\Hill");

            base.LoadContent();
        }

        private void InitializeGeonBitUI()
        {
            Button backButton = new Button("Back", ButtonSkin.Default, Anchor.TopLeft, new Vector2(140, 40), new Vector2(20, 20))
            {
                OnClick = (Entity sender) => { BackButton_OnClick(sender); }
            };

            backButton.ButtonParagraph.Scale = 0.7F;

            Panel panel = new Panel(new Vector2(500, 350));
            panel.AddChild(new Header("Game Over"));
            panel.AddChild(new HorizontalLine());
            panel.AddChild(new Paragraph("Waves Completed: " + WavesCompleted));
            panel.AddChild(new Paragraph("Creeps Killed: " + CreepsKilled));
            panel.AddChild(new Paragraph("Gold Earned: " + GoldEarned));
            panel.AddChild(new Paragraph("Score : " + Score));

            UserInterface.Active.AddEntity(backButton);
            UserInterface.Active.AddEntity(panel);
        }

        private void BackButton_OnClick(Entity sender)
        {
            UserInterface.Active.Clear();
            Game.Components.Clear();
            Game.Components.Add(new StartMenuScene(Game));
        }
    }
}
