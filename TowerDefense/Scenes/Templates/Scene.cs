﻿namespace TowerDefense.Scenes.Templates
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Microsoft.Xna.Framework;

    internal class Scene : DrawableGameComponent
    {
        public Scene(Game game) : base(game)
        {
        }

        public Rectangle WindowBounds
        {
            get => GraphicsDevice.Viewport.Bounds;
        }

        public Point WindowSize
        {
            get => GraphicsDevice.Viewport.Bounds.Size;
            set
            {
                GraphicsDeviceManager graphics = Game.Services.GetService(typeof(GraphicsDeviceManager)) as GraphicsDeviceManager;
                graphics.PreferredBackBufferWidth = value.X;
                graphics.PreferredBackBufferHeight = value.Y;
                graphics.ApplyChanges();
            }
        }
    }
}
