﻿namespace TowerDefense.Scenes
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using GeonBit.UI;
    using GeonBit.UI.Entities;
    using Microsoft.Xna.Framework;
    using Microsoft.Xna.Framework.Graphics;
    using TowerDefense.Managers;
    using TowerDefense.Managers.Templates;
    using TowerDefense.Objects;
    using TowerDefense.Scenes.Templates;

    internal class GameplayScene : Scene
    {
        private SpriteBatch _spriteBatch;
        private List<Manager> _managers;

        public GameplayScene(Game game, string selectedFileName) : base(game)
        {
            _managers = new List<Manager>
            {
                new MapManager(this, selectedFileName),
                new TowerManager(this),
                new UiManager(this),
                new WaveManager(this),
                new BulletManager(this),
                new ParticleManager(this),
                new PlayerManager(this)
            };
        }

        public override void Draw(GameTime gameTime)
        {
            UserInterface.Active.Draw(_spriteBatch);

            GraphicsDevice.Clear(Color.CornflowerBlue);

            _spriteBatch.Begin(blendState: BlendState.AlphaBlend);
            _managers.ForEach(e => e.Draw(_spriteBatch));
            _spriteBatch.End();

            UserInterface.Active.DrawMainRenderTarget(_spriteBatch);

            base.Draw(gameTime);
        }

        public T GetManager<T>() where T : Manager
        {
            return _managers.Find(e => e is T) as T;
        }

        public override void Initialize()
        {
            _managers.ForEach(e => e.Initialize());

            base.Initialize();
        }

        public override void Update(GameTime gameTime)
        {
            if (!Game.IsActive)
            {
                return;
            }

            UserInterface.Active.Update(gameTime);

            _managers.ForEach(e => e.Update(gameTime));

            if (GetManager<PlayerManager>().Life == 0)
            {
                UserInterface.Active.Clear();
                Game.Components.Clear();
                UnloadContent();

                GameOverScene gameOverScene = new GameOverScene(Game)
                {
                    CreepsKilled = GetManager<WaveManager>().KilledEnemies,
                    GoldEarned = GetManager<PlayerManager>().Gold,
                    Score = GetManager<PlayerManager>().Score,
                    WavesCompleted = GetManager<WaveManager>().WavesCompleted
                };

                Game.Components.Add(gameOverScene);
            }

            base.Update(gameTime);
        }

        protected override void UnloadContent()
        {
            TextureManager.Textures.Clear();
            base.UnloadContent();
        }

        protected override void LoadContent()
        {
            TextureManager.Textures[typeof(Giant)] = Game.Content.Load<Texture2D>(@"Enemies\Giant");
            TextureManager.Textures[typeof(GunTower)] = Game.Content.Load<Texture2D>(@"Towers\GunTower");
            TextureManager.Textures[typeof(CannonTower)] = Game.Content.Load<Texture2D>(@"Towers\CannonTower");
            TextureManager.Textures[typeof(Cannonball)] = Game.Content.Load<Texture2D>(@"Bullets\Cannonball");
            TextureManager.Textures[typeof(QuickBullet)] = Game.Content.Load<Texture2D>(@"Bullets\QuickBullet");
            TextureManager.Textures[typeof(Explosion)] = Game.Content.Load<Texture2D>(@"Particles\Explosion");

            _spriteBatch = new SpriteBatch(GraphicsDevice);
            _managers.ForEach(e => e.LoadContent());

            base.LoadContent();
        }
    }
}
